export const state = () => ({
  logged: false,
  user: ''
})

export const getters = {
  loginStatus: state => state.logged,
  username: state => state.user
}

export const actions = {
  saveLoginStatus({ commit }, { logged, user }) {
    commit('SAVE_LOGIN_STATUS', { logged, user })
  },
  closeSession({ commit }) {
    commit('CLOSE_SESSION')
  }
}

export const mutations = {
  SAVE_LOGIN_STATUS(state, { logged, user }) {
    state.logged = logged
    state.user = user
  },
  CLOSE_SESSION(state) {
    state.logged = false
    state.user = ''
  }
}
