export const state = () => ({
  showAbout: false
})

export const getters = {
  getModalStatus: state => el => state[el]
}

export const actions = {
  toggleModal({ commit }, { modalId, status }) {
    commit('TOGGLE_MODAL', { modalId, status })
  }
}

export const mutations = {
  TOGGLE_MODAL(state, { modalId, status }) {
    state[modalId] = status
  }
}
