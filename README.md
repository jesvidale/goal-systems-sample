# goalSystems

> Vue project for goalSystems

## 1. Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


## 2. About project

Framework: Vue
Environment: Webpack (Nuxt)

This technologies were chosen in order to accomplish with ultimate web composition stantdards. Web components is a must nowadays, which make projects more extensible in the future. In addition, it is working with flux pattern.

Every component has its logic. Three kind of components can be sorted this way:

- Layout (general layout)
- Pages (views)
- Components (with its own functionallity)

For login process, flux pattern eases the process. Data gets saved into a store after calling a mocked service. That store is configured to have persistent data. Had the project backend, the idea would be to provide a session key with expiration to get a robust system.

## 3. Requirements

There are many libraries for special components in project. The ones proposed are not suposed to be the best, because they are not the best optimized for Vue as their official websites for their usage describes. There are many others such as Vuetify or Vue Material..., that are fully written in Vue. I did not like Materialize.css because it lies on jQuery. No way, they were included as the requirements says.

### 3.1 Mistakes

1. At login, menu should not be shown. It has no sense showing that button if a non logged user is not able to browse into the options. It got removed at login page at this version.

2. Missalignment at heading bar for responsive solutions. It got corrected at this version.

3. Spanglish. Spanish and English get mixed in the same app.

### 3.2 UX improvements

1. Providing 'active' status to elements at navigation bar. It helps user to browse.

2. Giving title at the world icon at heading bar. Its purpose is not clear.

3. Setting 'hover' status at dropdowns.
   
### 3.3 Design improvements

1. At events list, there are two flows. Date selection and options. I would propose separating them into two rows depending on their importance rather than having them in the same row. I guess it stands out more date selection, that are the the primary selection. Options are secondary and depends on the primary selection; should be below.

2. Breadcrumbs at mobile resolution shoud better go below page title. Long breadcrumbs would overlap with title. This way, design gets unified and clearer. (Implemented)

3. Unify buttons design. Some of them have icon and some others dont. It is visible at events view with filters.
