import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'goalSystems',
    paths: ['login']
  })(store)
}
